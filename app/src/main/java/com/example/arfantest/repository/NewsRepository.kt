package com.example.arfantest.repository

import com.example.arfantest.api.RetrofitInstance
import com.example.arfantest.db.ArticleDatabase

class NewsRepository(
    val db: ArticleDatabase
) {
    suspend fun getBreakingNews(countryCode: String, pageNumber: Int) =
        RetrofitInstance.api.getBreakingNews(countryCode, pageNumber)
}